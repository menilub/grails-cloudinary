grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.repos.infinitOpensource.url = "https://api.bintray.com/maven/infinit/infinit-opensource/grails-cloudinary"
grails.project.repos.default = "infinitOpensource"
grails.project.dependency.resolver = "maven" // or ivy
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }

    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'

    repositories {
        grailsCentral()
        mavenLocal()
        mavenCentral()
    }

    dependencies {
        compile 'com.cloudinary:cloudinary:1.0.13'
    }

    plugins {
        build(":release:3.0.1",":rest-client-builder:1.0.3") {
            export = false
        }
    }
}
